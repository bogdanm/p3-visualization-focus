%
%  p3
%
%  Created by Bogdan Marculescu on 2013-05-03.
%  Copyright (c) 2013 . All rights reserved.
%
\documentclass[conference]{IEEEtran}

% Use utf-8 encoding for foreign characters
\usepackage[utf8]{inputenc}

% Setup for fullpage use
\usepackage{fullpage}



% Surround parts of graphics with box
\usepackage{boxedminipage}

% Package for including code in the document
\usepackage{listings}

% If you want to generate a toc for each chapter (use with book)
%\usepackage{minitoc}

% This is now the recommended way for checking for PDFLaTeX:
\usepackage{ifpdf}

\ifpdf
\usepackage[pdftex]{graphicx}
\else
\usepackage{graphicx}
\fi

\hyphenation{op-tical net-works semi-conduc-tor}

\begin{document}

\title{Visualizing Interactive Search-Based Software Testing}
\author{ 
\IEEEauthorblockN{Bogdan Marculescu\IEEEauthorrefmark{1}, Robert Feldt\IEEEauthorrefmark{1}\IEEEauthorrefmark{2}, Richard Torkar\IEEEauthorrefmark{1}\IEEEauthorrefmark{2}}
\IEEEauthorblockA{\IEEEauthorrefmark{1}Blekinge Institute of Technology\\
School of Computing\\
Karlskrona, Sweden\\}

\IEEEauthorblockA{\IEEEauthorrefmark{2}Chalmers and University of Gothenburg\\
Dept.\ of Computer Science and Engineering\\
Gothenburg, Sweden\\}
}


\date{}

\ifpdf
\DeclareGraphicsExtensions{.pdf, .jpg, .tif}
\else
\DeclareGraphicsExtensions{.eps, .jpg}
\fi

\maketitle


\begin{abstract}
	
	Interactive search-based testing systems generate large amounts of information that has to be analyzed, interpreted, and acted upon by human engineers. A good understanding of the current solution candidates, based around a good visualization approach, is thus essential to the proper functioning of such systems. In this paper, we propose an interactive search-based testing system that allows interaction with domain specialists and proposes a visualization approach aimed specifically at non-software engineers.
	
\end{abstract}

\section{Introduction} % (fold)
\label{sec:introduction}

	Software is often developed as one component of complex systems. One of the implications of this situation is that the software in question must operate under domain-specific, and often implicit, constraints and limitations. In such situations, the quality of the system does not depend on the quality of the software alone, but on trade-offs between the level of quality of different components and the costs associated ensuring that quality. Therefore, domain specialists are an integral part of software development, using their knowledge and experience to make those trade-offs. This kind of domain knowledge is often a more important factor than expertise in software testing.
	
	In order to enable domain specialists to efficiently and effectively test the software being produced, a software testing tool must be both flexible and easy to interact with and guide. The flexibility to allow for changes in the focal point of the software testing process can be achieved by employing search-based software testing. Successfully guiding that such a system relies on the the proper visualization of the current state of the search and of the current candidates. 
	
	This paper proposes an interactive search based software testing system that allows for interaction with domain specialists, without requiring the latter to develop an expertise in software testing. The focus will be on displaying information that is meaningful to the domain specialist, that allow them to understand the current state of the search and enables them to guide further efforts; thus allowing the domain specialists to use their knowledge of the system and the context, their own definitions of the relevant quality criteria, and their own prioritization of those criteria.

	In section~\ref{sec:related_work} we present existing efforts with respect to interactive evolutionary search and the visualization choices they entail, and discuss how our approach differs from them. Section~\ref{sec:context} describes the industrial context that forms the basis of this work and describe a running example system used to illustrate our concepts. Our approach is described in section~\ref{sec:visualization_goal} and a brief evaluation provided in section~\ref{sec:evaluation}. Section~\ref{sec:conclusions} concludes the paper.
	

% section introduction (end)

\section{Related Work} % (fold)
\label{sec:related_work}


A term coined by Harman and Jones in 2001~\cite{Harman2001}, search based software engineering (SBSE) is the application of metaheuristic search techniques to software engineering problems, e.g.\ ~\cite{XanthakisESLKK92,Feldt99,HarmanMZ09}. Search based software testing (SBST) is a branch of SBSE that deals with testing problems and has successfully been applied to several types of testing problems~\cite{McMinn2011,AfzalTF09}, from object-oriented containers~\cite{Arcuri20083075} to dynamic programming languages~\cite{Mairhofer:2011:SST:2001576.2001826}.

Feldt~\cite{Feldt02,Feldt99} and Parmee et al~\cite{parmee2000multiobjective} considered the use of interactive search to explore engineering designs, provide better understanding of domain constraints and guide the search. In terms of visualization, Kerren~\cite{Kerren2005} proposes a tool for displaying different levels of data regarding a running evolutionary algorithm. 

Takagi defines Interactive Evolutionary Computation as `an EC that optimizes systems based on subjective human evaluation'~\cite{Takagi2001}. This approach relies on human interaction to evaluate the solutions being developed by the interactive search system, allowing for situations where the choice of solution is dependent on `human preference, intuition, emotion and psychological aspects'~\cite{Takagi2001}. The original paper refers to art and animation, graphics and image processing; in general applications where the evaluation of a candidate has a strong subjective component. Nevertheless, we think that this can be generalized to any type of candidate evaluation where not all the necessary information, e.g.\ domain specific knowledge, experience, background information, or intuition, can be modeled into the system or encoded in an automatic evaluation approach.

Interactive search benefits from the domain knowledge of the engineer using such a system. To do so, however, the system must display information in a way that is understandable to the domain specialist using the system, that conforms to domain specific representations and concepts, and that requires little or no additional skills to be acquired. 

Our system differs from previous approaches precisely in the issue of using the current domain specific representation as a base for interaction, while trying to shield the domain specialist from the software engineering specific details.




% section related_work (end)

\section{Context} % (fold)
\label{sec:context}

This section describes the context of the current work and some of the constraints that act upon the system.

Our industrial partner develops complex systems, containing embedded software, but where software is not the main component. In addition, they provide their customers with tool to modify and develop their own embedded software for the hardware provided. The customers in question operate in similar, but not identical contexts and, as a result, knowledge of the domain, its peculiarities and restrictions, is of utmost importance. In such a context, the quality of the resulting system depends on that of the software, but not exclusively so. A domain specialist can use their knowledge of the domain, and experience with the limitations of requirements of the systems and components being used, to better assess the quality characteristics of the complete product. 

To better illustrate the challenges we face and the solutions we propose, we will present a running example: that of a control mechanism for an electric motor, e.g.\ powering a mechanical arm. An embedded software filter ensures that the power input to the motor does not exceed acceptable limits and that there are no sudden changes in the power levels that can bring about damage to the motor itself or to other components in the system.

The system-under-test is the aforementioned average filter, that transforms sudden changes in the control mechanisms, e.g.\ a sudden acceleration, into a smoother signal that can be used as an input for the motor. 

The first identified quality objective is, therefore, ensuring that the output signal is free from potentially damaging discontinuities. This is encoded in our system as the quality criterion Discontinuities, describing the number of discontinuities discovered, and that the ISBST system tries to maximize.

The second quality objective is that of ensuring that the output of the average filter does not exceed the maximum value allowable for the safe operation of the motor itself. This is described in the system as the Upper Limit. It describes the degree to which the output signal exceeds the given limit, cumulative for the entire signal. This objective is to be maximized.

A third quality objective is ensuring that test cases are short enough to be easily evaluated and understood by the domain specialists. The objective, encoded as the output signal Length, is to be minimized.

To gain a better understanding of the situation, we conducted discussions with our industrial partner and attended a training session for our customer's and their clients' engineers. The approach is, therefore, to provide software engineering training to domain specialists and to rely on them to develop the necessary embedded software. This approach validates our notion that domain knowledge and experience are the key factors in this type of context, and guided our choices to develop a support tool for domain specialists. The visualization approach being used must, therefore, have the flexibility to allow for the fact that domain specialists all come from different, albeit related, fields and are developing different products. 

We have developed an Interactive Search Based Software Testing system, tailored to the specific need of our industrial partner, and based on the type of software application that they usually develop. The quality criteria we have used to evaluate this system and the resulting test cases are based on the our discussions with our industrial partners, their current practices and their experiences. 

% section context (end)

\section{Visualization} % (fold)
\label{sec:visualization_goal}

In this section we will first present a brief overview of our system. This will be followed by a closer look at our approach to the visualization of test cases.

\subsection{Overview of the System} % (fold)
\label{sub:overview_of_the_system}

The interactive search based software testing system (ISBST) we have envisioned relies on involving the domain specialist in the test case development process, allowing them to use their knowledge and expertise to guide the search, while at the same time separating them from the minutiae of the underlying software system. This means that the specialists in question will continue to work with concepts and ideas familiar to them and do not need to develop additional skills to perform their assignments. 

\begin{figure}[htbp]
	\centering
		\includegraphics[scale=0.5]{p3-v1.pdf}
	\caption{Overview of the ISBST}
	\label{fig:p3-v1}
\end{figure}

The inner cycle contains the search based software testing system that forms the basis of our approach. The domain specialist guides the search indirectly, by selecting the quality criteria that they consider important at a given time and prioritizing them. The fitness function is then adapted to reflect those priorities.

The outer cycle handles the interaction with the domain specialist, including quality criterion selection and prioritization, and the visualization of the candidates.

In his paper, Takagi~\cite{Takagi2001} identifies user fatigue as a main problem in using interactive evolutionary computation. He also proposes some methods of alleviating the problems. 

In our system, the outer cycle handles any issues regarding the interaction with the domain specialist, including the prevention of user fatigue. To this end, some of the methods presented in~\cite{Takagi2001} have been adopted. Only some of the large number of solutions being generated are displayed, with the selection being performed on the basis of the priorities the domain specialist has stated. Interaction events take place once every 500 optimization steps, to further alleviate the problem of fatigue. 


Our stated goal for this system is to enable domain specialists to use their knowledge and experience to create test cases for the systems they are developing, without the need to acquire specific skills related to search based software testing systems in particular or to software engineering in general. To this end, two issues guide our selection of visualization approaches:

\begin{itemize}
	\item \textbf{Compatibility} involves using a visualization approach that is compatible with the current approaches and that conforms to the concepts being currently used in the field.
	\item \textbf{Concern separation} between those issues that are related to candidate quality and candidate representation, and those issues only relevant for the underlying search based system. The former are displayed to the domain specialist for day-to-day use, while the latter are abstracted away.
\end{itemize}

All of the options presented below refer to means of visualizing candidates or groups of candidate, in terms of concepts familiar to the domain specialist. 

% subsection overview_of_the_system (end)

\subsection{Visualizing the candidate population} % (fold)
\label{sub:visualizing_the_candidate_population}

	As stated previously, an ISBST system generates a large number of candidate solutions. This raises the issue of selecting which of those solutions the domain specialist will see. One option is to prepare a ranking, based on the quality criteria prioritization provided by the specialist. 
	
	This approach, however, restricts the access to candidates that are not chosen for display, but may have desirable characteristics or may represent interesting trade-offs. It creates, in effect, a confirmation bias: it displays only those candidates that perform best with respect to the prioritization selected by the domain specialist and reinforces the notion that the prioritization is accurate by hiding candidates that do not rank well in that evaluation.
	
	\begin{figure}[htbp]
		\centering
			\includegraphics[scale=0.22]{p3-v2.png}
		\caption{The visualization of the entire candidate population}
		\label{fig:p3-v2}
	\end{figure}
	
	The approach we adopted, exemplified in figure~\ref{fig:p3-v2}, is to allow the domain specialist to access all the candidates. The candidates are displayed in a graph. Two of the quality criteria selected are chosen as axes for the graph, while a third quality criterion may be represented by means of color coding or symbol size. This allows the domain specialist to get a quick overview of all the candidates present in the population, and especially those candidates that are remarkable in some way: maximizing the values for one or more of the quality criteria, centers of certain clusters, or candidates that are not part of any cluster.
	
	By displaying all the candidates present in the population at a given moment, we avoid the confirmation bias described above. Some candidates will still be hidden, especially those that are part of large clusters or that overlap with other candidates. Those candidates are, however, equivalent from most perspectives and any one candidate from such a tight cluster is representative of the entire cluster.
	
	Overall, this approach allows the domain specialist to select, for further investigation, those candidate solutions that are remarkable, not just in light of the stated quality objective prioritization, but also in light of their own knowledge, experience, and intuition.
	
% subsection visualizing_the_candidate_population (end)

\subsection{Visualizing a candidate} % (fold)
\label{sub:visualizing_a_candidate}

The approach presented in section~\ref{sub:visualizing_the_candidate_population} allows an overview of the entire test case candidate population. While this is useful in obtaining an general view of the available options, a more in-detail view of a selected candidate is required. This view can take several forms. We proposed four form of visualizing a single candidate.

%Should I add a section on the current visualization approach??

\textbf{Vertical Table.} The first proposed option is to display the values for the input and output signals as a table. Additional information available in the outer cycle can also be displayed, e.g.\ pass-fail status of the test case, or additional quality objectives that have been computed.

\begin{figure}[htbp]
	\centering
		\includegraphics[scale=0.5]{p3-v3.png}
	\caption{Vertical table view}
	\label{fig:p3-v3}
\end{figure}

The figure~\ref{fig:p3-v3} shows an example of the input and output values for one of the test cases developed for our average filter example. 

The main benefit of this approach is that it is based on approaches currently being used to display manually developed test cases. This fits in with the requirement of using, as far as possible, existing concepts and visualization mechanisms.

\textbf{Horizontal Table.} An alternative of the above, the table is displayed horizontally, rather than vertically. This takes this representation away from the current vertical table view, and brings it closer to the graphical representation of a signal.

\begin{figure}[htbp]
	\centering
		\includegraphics[scale=0.34]{p3-v31.png}
	\caption{Horizontal graph view}
	\label{fig:p3-v31}
\end{figure}

The approach is illustrated in figure~\ref{fig:p3-v31}. The current implementation has both table views being shown as tooltips, not as a replacement for other visualization options.


\textbf{Simple Graph View.} A more intuitive option is to display the input and output signals as overlapping graphs, as in figure~\ref{fig:p3-v4}. In our example, the red line represents the input and the blue line the output of the average filter. 


\begin{figure}[htbp]
	\centering
		\includegraphics[scale=0.22]{p3-v4.png}
	\caption{Simple graph}
	\label{fig:p3-v4}
\end{figure}

This view allows an at-a-glimpse comparison between the signals and enables the domain specialist to quickly spot any discrepancies between the system behavior and their expectations. This type of signal representation is quite standard and in line with the other tools being used by the company. 

\textbf{Graph View Highlighting Differences.} A graph displaying more information is shown in figure~\ref{fig:p3-v5}. The differences between the signals are highlighted and, in both this and the Simple Graph View, values for individual notes can be visualized in detail. 

\begin{figure}[htbp]
	\centering
		\includegraphics[scale=0.22]{p3-v5.png}
	\caption{Graph with highlighted differences}
	\label{fig:p3-v5}
\end{figure}

The idea behind this approach is to further allow a quick evaluation of the behavior of the system to determine whether or it meets expectations. Our industrial partners and their customers, often use signal representations and graphical signal representations in their day-to-day development activities, so the visualization approach should be familiar.

% subsection visualizing_a_candidate (end)



% section visualization_goal (end)


\section{Evaluation} % (fold)
\label{sec:evaluation}



\subsection{Evaluation Goal and Approach} % (fold)
\label{sub:evaluation_goal}

The main goals of the evaluation efforts described in this section are as follows:

\begin{itemize} 
	\item to confirm our interpretation of the current means by which a test case is visualized, 
	\item and to determine which, if any, of the visualization options presented above is useful and has value for the domain specialists. 
\end{itemize}

Our interpretation of current visualization means forms the basis for our understanding, both of the system in general and of the information needs of the domain specialists, in particular. Therefore, confirming that this interpretation is as accurate as possible is a constant concern. 

The system we are developing offers the benefit of extensibility. New approaches to visualization can be developed and made available at later dates, if proven to be needed or useful. Nevertheless, a good initial selection of visualization options will form a good starting point for further attempts to understand the information need of domain specialists, and allow a better evaluation of the underlying ISBST system.

As a preliminary evaluation of our visualization choices, a questionnaire was sent to four specialists working with our industrial partner. These are specialists working on a development environment used for embedded software. They were selected because of their familiarity with the domain, the tools being used, and the information needed to develop software in that particular context. The low number of respondents is due to the unique set of skills we required to obtain the information we needed. 

The first section was concerned with confirming the accuracy our understanding of how manually created test cases are currently represented. The second section contained a set of three statements about each of the options presented above:

\begin{itemize}
	\item This would be a useful representation
	\item This visualization provides information that is unavailable otherwise
	\item This visualization provides information that you would use on a regular basis
\end{itemize}

The respondents were asked to give a reaction to these statements, ranging from strongly agree to strongly disagree. Open ended questions allowed all respondents to provide additional information, if they felt this was needed. 

% subsection evaluation_goal (end)

\subsection{Results} % (fold)
\label{sub:results}

The results of these evaluation efforts are as follows. 

\textbf{The current visualization choices.} Our assumptions regarding the current visualization of test cases were confirmed. This statement needs to be qualified due to two different issues that impact these results. 

First of all, our interviews were with developers of an embedded software development tool. Their activities include testing procedures, but their main focus is on the development of the tool itself. This was a conscious choice, as mentioned before, since it gives us access to knowledge regarding other development aspects than those strictly related to testing. It does, however, mean that they cannot speak on behalf of all the employees involved in software quality assurance within the company. 

Moreover, our industrial partner's clients, that the ISBST system is ultimately aimed at, may have non-standard testing procedures and visualization approaches. 

That said, our interpretation of the results is that our understanding of the current situation, while certainly not perfect or complete, is sufficient to allow us to proceed further with developing the system and our ideas.

\textbf{The proposed visualization choices.} The visualization choices we proposed as part of our system were met with a mixed reception. 

Two of the proposed options were met with a favorable response. The vertical table view and the simple graph view have received a positive review. This is not unexpected, as these two views are close to those currently used to describe signals in general and test cases in particular. This proximity to current approaches is relevant because the specialists' familiarity will improve their opinion of that particular visualization approach. These findings confirm our decision to include these views in any visualization toolbox to be used with our system.

The visualization of the entire candidate population had a more mixed reception, with some approval and some disapproval. We interpret this to mean that the visualization has some uses, and could provide some interesting information, but it does need to be polished further. A possible reason for the view being regarded as not being particularly useful was that the questions were focused on the visualizations, leaving additional details relating to the functioning of the system aside. This means that the relevant of some of the information provided, e.g.\ relating to the fitness value for priority objectives, may have not been regarded as useful as they might otherwise have been. Further studies will be performed, to assess the usefulness of this approach in the context it is meant to be used.

The last two options, the horizontal table and the graph with the highlighted differences, have received a neutral, although somewhat negative, reception. This reception can be attributed to the lack of familiarity and to displaying information that is not immediately relevant. These views will mostly be dropped from further efforts, unless other requests emerge. 

Overall the results indicate that the visualizations are useful and informative overall, with some being more so than others. While there are threats to any conclusions that can be drawn from this evaluations, threats that will be discussed in the following section, the evaluation does give the confidence that the options selected are plausible and indicates further improvements that can be made, both to the system and to future evaluation efforts.



% subsection results (end)

\subsection{Threats to Validity} % (fold)
\label{sub:validity_threats}

Any discussion of the evaluation must deal with some of the threats to validity encountered in the effort.

\textbf{Inherent threats in the evaluation tool.} First of all, the issue of the evaluation tool needs to be discussed. The scale of the evaluation tool is ordinal, rather than interval. In effect, while we can state clearly that one level of agreement is higher or lower than another, we cannot state confidently that the intervals between agreement and strong agreement is the same as that between neutrality and agreement. This is a problem especially if the results are to be evaluated quantitatively. Since this is a preliminary study, primarily aimed at gaining further understanding and obtaining information for further studies, this threat does not affect the conclusions drawn here.

A second potential threat is that each respondent may have a different evaluation of the same situation. To alleviate this problem, further interviews and studies will be conducted and cross-references against the findings of this evaluation. Since the conclusions being drawn on the basis of these results are tentative and meant to guide further efforts rather than form strict conclusions, this is not as significant a threat as might seem.

A third threat relating to the evaluation tool lies in potential misunderstandings in the way questions were understood by the respondents. This is always a concern when formulating questions and care was taken to ensure the questions were clear and straightforward. That said, potential misunderstandings have already been identified and will be corrected in future evaluations. 

\textbf{Threats due to the number of respondents.} As state earlier, the evaluation was conducted on four specialists working with our industrial partner. From an evaluation perspective, this is too low a number of respondents to draw any clear and hard conclusions. However, our goal was to gain a better understanding of the types of visualization that might be relevant to our industrial partner and to other companies operating is a similar context. In light of this goal, the respondents' qualifications and domain expertise is a far greater benefit to the accuracy and relevance of our conclusions than the numbers might indicate. 

\textbf{The threat of source bias.} Our evaluation is based  on four respondents, all employees of the same company and working with the same techniques, tools and methods. This introduces the threat that there might be peculiarities of that context that do not generalize to the entire domain, the respondents might not be representative of the population. The first response is that, since the conclusions from this evaluation are just guidelines for further development and evaluation efforts, such a bias, if present, could easily be removed by further efforts. In the short term, the information gained outweighs the potential inaccuracies. A second response to this threat is that confidence in the information provided by the respondents is increased by them actively working with tools and concepts that are widely used in their domain. Moreover, while there are certain areas where their knowledge may not be representative, they are keenly aware of those areas and took care to point them out during the evaluation. These areas of uncertainty will receive greater attention in further development and evaluation efforts.

% subsection validity_threats (end)

% section evaluation (end)

\section{Conclusions} % (fold)
\label{sec:conclusions}

This paper has presented an Interactive Search Based Software Testing (ISBST) system, that uses visualization as a primary means of allowing domain specialists to use their domain knowledge and experience to guide the search. 

We have proposed a number of visualizations of test case candidates and conducted an initial evaluation of their usefulness. The findings of this evaluation will form the basis for further development and evaluation efforts. 

In conclusion, some of the visualization options presented here have been evaluated as useful and meaningful by specialists in the field. Those options have, therefore, been selected as bases for further study. This provides early support for our concepts of enabling domain specialists to guide an ISBST system and indicates which visualization have the most potential to allow this goal to be achieved.

% section conclusions (end)

\bibliographystyle{IEEEtran}
\bibliography{p2}

\end{document}
